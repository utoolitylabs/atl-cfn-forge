AWSTemplateFormatVersion: 2010-09-09
Description: Atlassian Cloudformation Forge

Metadata:
  AWS::CloudFormation::Interface:
      ParameterGroups:
        - Label:
            default: Network Config
          Parameters:
            - CidrBlock
            - InternetAccessible
            - ExternalSubnet
            - InternalSubnet
            - VPC
        - Label:
            default: EC2 Config
          Parameters:
            - KeyName
            - NodeInstanceType
            - NodeVolumeSize
        - Label:
            default: DNS Config
          Parameters:
            - HostedZone
        - Label:
            default: Forge config
          Parameters:
            - Analytics
            - CustomTemplateRepo
            - CustomTemplateGitKey
            - LocalAnsibleGitRepo
            - LocalAnsibleGitSshKeyName
            - Nodes
            - FlaskSecretKey
            - Regions
            - SamlMetadataUrl
      ParameterLabels:
        Analytics:
          default: Enable analytics
        CustomTemplateRepo:
          default: Custom Cloudformation template repository
        CustomTemplateGitKey:
          default: Git key for custom template repository
        FlaskSecretKey:
          default: Flask secret key
        ExternalSubnet:
          default: External subnet Id
        InternalSubnet:
          default: Internal subnet Id
        InternetAccessible:
          default: Load balancer accessible from the internet
        CidrBlock:
          default: IP range permitted to access forge
        HostedZone:
          default: Route 53 Hosted Zone
        KeyName:
          default: Key Pair
        LocalAnsibleGitRepo:
          default: Git repo for local-ansible
        LocalAnsibleGitSshKeyName:
          default: Git key for local-ansible
        Nodes:
          default: Number of nodes
        NodeInstanceType:
          default: Node Type
        NodeVolumeSize:
          default: Node Volume Size
        Regions:
          default: Regions to operate in
        SamlMetadataUrl:
          default: SAML metadata URL
        VPC:
          default: VPC Id

Parameters:
  Analytics:
    Default: true
    AllowedValues:
      - true
      - false
    ConstraintDescription: Must be true or false
    Description: Enable analytics to be sent back to Atlassian
    Type: String
  CidrBlock:
    Default: '0.0.0.0/0'
    AllowedPattern: '(\d{1,3})\.(\d{1,3})\.(\d{1,3})\.(\d{1,3})/(\d{1,2})'
    ConstraintDescription: Must be a valid IP CIDR range of the form x.x.x.x/x.
    Description: The CIDR IP range that is permitted to access the Service URL. Use 0.0.0.0/0 if you want public access from ALL places in the internet.
    Type: String
    MinLength: 9
    MaxLength: 18
  CustomTemplateRepo:
    Description: To make additional Cloudformation templates available in Forge, provide repo location to git clone (ie git@bitbucket.org:atlassian/custom-templates.git)
    Type: String
  CustomTemplateGitKey:
    Description: If your custom template repo is private, provide a AWS Systems Manager ParameterStore key name that holds an ssh private key that can access the repo
    Type: String
  ExternalSubnet:
    ConstraintDescription: Select one Subnet ID within the selected VPC
    Description: Subnet where your user-facing load balancer will be deployed. MUST be within the selected VPC.
    Type: List<AWS::EC2::Subnet::Id>
  FlaskSecretKey:
    Description: 'Override the default secret key passed to Flask app to enable sessions, which are required to run. For more info: https://bit.ly/2PRfJRk'
    NoEcho: True
    Type: String
  HostedZone:
    Default: 'myteam.example.com.'
    ConstraintDescription: Must be the name of an existing Route53 Hosted Zone.
    Description: The domain name of the Route53 Hosted Zone in which to create cnames
    Type: String
  InternalSubnet:
    ConstraintDescription: Select one Subnet ID within the selected VPC
    Description: Subnet where your cluster nodes and other internal infrastructure will be deployed. MUST be within the selected VPC. Specify the ExternalSubnet again here if you wish to deploy the whole stack into the same subnet.
    Type: List<AWS::EC2::Subnet::Id>
  InternetAccessible:
    AllowedValues:
      - true
      - false
    ConstraintDescription: Must be 'true' or 'false'.
    Default: false
    Description: Whether the load balancer can be accessed from the internet
    Type: String
  KeyName:
    ConstraintDescription: must be the name of an existing EC2 KeyPair.
    Description: Name of an existing EC2 KeyPair to enable SSH access to the instances
    Type: AWS::EC2::KeyPair::KeyName
  LocalAnsibleGitRepo:
    Default: ''
    Description: For additional node customization, provide ansible repo location to git clone. This may be in either http or ssh format, but it must be in the correct format for your git hosting provider (as it is passed directly to a 'git clone' command), eg git@bitbucket.org:atlassian/atlassian-local-ansible-example.git if using Bitbucket Cloud, or ssh://git@bitbucketserver.example.com:7997/my-local-ansible.git (ie protocol included) if using Bitbucket Server.
    Type: String
  LocalAnsibleGitSshKeyName:
    Default: ''
    Description: if your git repo is private, provide a AWS Systems Manager ParameterStore key name that holds an ssh private key that can access the repo
    Type: String
  Nodes:
    Default: 1
    AllowedValues:
      - 1
      - 0
    ConstraintDescription: Must be a number
    Description: Number of Forge nodes. Set 0 to destroy the existing Forge node, eg when you need to roll out configuration changes.
    Type: Number
  NodeInstanceType:
    Default: t2.medium
    AllowedValues:
      - t2.medium
      - t2.large
      - m5.large
      - m5.xlarge
    ConstraintDescription: Must be an EC2 instance type from the selection list
    Description: Instance type for the application nodes
    Type: String
  NodeVolumeSize:
    Default: 50
    Description: Size of the root EBS volume on application nodes
    Type: Number
  Regions:
    Default: 'us-east-1: N.Virginia, us-west-2: Oregon'
    ConstraintDescription: Must be a list of AWS regions
    Description: "Comma delimited list of the regions you want Forge to operate in, including display names for the region, in format 'aws_region: region_name'. Names are for reference only, so can be AWS region names or Staging/Production etc. First region will be the default region."
    Type: CommaDelimitedList
  SamlMetadataUrl:
    Description: Metadata URL for your SAML provider
    Type: String
  VPC:
    Default: vpc-dd8dc7ba
    ConstraintDescription: Must be the ID of a VPC.
    Description: Virtual Private Cloud (VPC)
    Type: AWS::EC2::VPC::Id

Mappings:
  RegionAmiMap:
    ap-northeast-1:
      "ami": "ami-06cd52961ce9f0d85"
    ap-northeast-2:
      "ami": "ami-0a10b2721688ce9d2"
    ap-south-1:
      "ami": "ami-0912f71e06545ad88"
    ap-southeast-1:
      "ami": "ami-08569b978cc4dfa10"
    ap-southeast-2:
      "ami": "ami-09b42976632b27e9b"
    ca-central-1:
      "ami": "ami-0b18956f"
    eu-central-1:
      "ami": "ami-0233214e13e500f77"
    eu-west-1:
      "ami": "ami-047bb4163c506cd98"
    eu-west-2:
      "ami": "ami-f976839e"
    eu-west-3:
      "ami": "ami-0ebc281c20e89ba4b"
    sa-east-1:
      "ami": "ami-07b14488da8ea02a0"
    us-east-1:
      "ami": "ami-0ff8a91507f77f867"
    us-east-2:
      "ami": "ami-0b59bfac6be064b78"
    us-west-1:
      "ami": "ami-0bdb828fd58c52235"
    us-west-2:
      "ami": "ami-a0cfeed8"

Conditions:
  InternetAccessible:
    !Equals [!Ref InternetAccessible, true]
  UseHostedZone:
    !Not [!Equals [!Ref HostedZone, '']]
  NoSamlMetadata:
    !Equals [!Ref SamlMetadataUrl, '']

Resources:
  ForgeRole:
    Type: AWS::IAM::Role
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service: [ec2.amazonaws.com]
            Action: ['sts:AssumeRole']
      ManagedPolicyArns:
        - arn:aws:iam::aws:policy/AmazonSSMReadOnlyAccess
      Path: /
      Policies:
        - PolicyName: ForgeNodePolicy
          PolicyDocument:
            Version: 2012-10-17
            Statement:
              - Action:
                - autoscaling:*
                - cloudformation:*
                - cloudwatch:DeleteAlarms
                - cloudwatch:PutMetricAlarm
                - ec2:*
                - elasticfilesystem:CreateFileSystem
                - elasticfilesystem:CreateMountTarget
                - elasticfilesystem:CreateTags
                - elasticfilesystem:DeleteFileSystem
                - elasticfilesystem:DeleteMountTarget
                - elasticfilesystem:DescribeFileSystems
                - elasticfilesystem:DescribeMountTargets
                - elasticloadbalancing:*
                - iam:AddRoleToInstanceProfile
                - iam:AttachRolePolicy
                - iam:CreateInstanceProfile
                - iam:CreateRole
                - iam:DeleteInstanceProfile
                - iam:DeleteInstanceProfile
                - iam:DeleteRole
                - iam:DeleteRolePolicy
                - iam:DetachRolePolicy
                - iam:PassRole
                - iam:PutRolePolicy
                - iam:RemoveRoleFromInstanceProfile
                - kms:CreateAlias
                - kms:CreateGrant
                - kms:CreateKey
                - kms:DescribeKey
                - kms:ListAliases
                - kms:PutKeyPolicy
                - kms:ScheduleKeyDeletion
                - kms:TagResource
                - kms:UpdateKey
                - rds:CreateDBInstance
                - rds:CreateDBSubnetGroup
                - rds:DeleteDBInstance
                - rds:DeleteDBSubnetGroup
                - rds:DescribeDBInstances
                - rds:DescribeDBSnapshots
                - rds:DescribeDBSubnetGroups
                - rds:ModifyDBInstance
                - rds:ModifyDBSubnetGroup
                - rds:RestoreDBInstanceFromDBSnapshot
                - route53:ChangeResourceRecordSets
                - route53:GetChange
                - route53:ListHostedZones
                - route53:ListResourceRecordSets
                - ssm:ListCommands
                - ssm:PutParameter
                - ssm:SendCommand
                Effect: Allow
                Resource: ['*']
              - Action:
                - s3:CreateBucket
                - s3:GetBucketAcl
                - s3:GetBucketTagging
                - s3:GetBucketVersioning
                - s3:ListBucket
                - s3:ListBucketByTags
                Effect: Allow
                Resource:
                  - !Sub ["arn:aws:s3:::atl-cfn-forge-${AccountId}", AccountId: !Ref "AWS::AccountId"]
              - Action:
                - s3:GetObject
                - s3:GetObjectAcl
                - s3:GetObjectTagging
                - s3:PutObject
                - s3:PutObjectAcl
                - s3:PutObjectTagging
                - s3:PutObjectVersionTagging
                Effect: Allow
                Resource:
                  - !Sub ["arn:aws:s3:::atl-cfn-forge-${AccountId}/*", AccountId: !Ref "AWS::AccountId"]

  ForgeNodeInstanceProfile:
    Type: AWS::IAM::InstanceProfile
    Properties:
      Path: /
      Roles: [!Ref ForgeRole]
# Forge instance config
  ForgeNodeGroup:
    Type: AWS::AutoScaling::AutoScalingGroup
    Properties:
      DesiredCapacity: !Ref Nodes
      LaunchConfigurationName: !Ref ForgeNodeLaunchConfig
      MaxSize: !Ref Nodes
      MinSize: !Ref Nodes
      LoadBalancerNames:
        - !Ref LoadBalancer
      VPCZoneIdentifier: !Ref InternalSubnet
      Tags:
        - Key: Name
          Value: !Sub "${AWS::StackName} Forge Node"
          PropagateAtLaunch: true
  ForgeNodeLaunchConfig:
    Type: AWS::AutoScaling::LaunchConfiguration
    Metadata:
      AWS::CloudFormation::Init:
        config:
          packages:
            yum:
              python36: []
              python36-pip: []
              amazon-ssm-agent: []
              xmlstarlet: []
              jq: []
              git: []
              xmlsec1: []
              xmlsec1-openssl: []
          files:
            /etc/atl:
              content: !Sub
                - |
                  ATL_AWS_STACK_NAME=${AWS::StackName}
                  ATL_AUTOSCALING_GROUP_NAME=ForgeNodeGroup
                  ATL_HOSTEDZONE=${HostedZone}
                - HostedZone: !Ref HostedZone
              group: root
              mode: "000640"
              owner: root
            /root/setup_forge.sh:
              content: !Sub
                - |
                  #!/usr/bin/env bash

                  # long paths are required for the atlassian-aws-deployment submodule to clone correctly
                  git config --global core.longpaths true

                  forgepath=/home/forge/atl-cfn-forge

                  if [ -d $forgepath ]; then
                      cd $forgepath
                      git pull
                  else
                      useradd -d /home/forge forge
                      cd /home/forge
                      git clone https://bitbucket.org/atlassian/atl-cfn-forge.git $forgepath
                      cd $forgepath
                  fi
                  /usr/bin/pip-3.6 install -r requirements.txt
                  mv /tmp/forge.properties $forgepath/forge.properties --force
                  mv /tmp/update $forgepath/update --force
                  chown -R forge:forge /home/forge
                  ./update

                  # create s3 bucket for forge objects
                  if aws s3api create-bucket --bucket atl-cfn-forge-${AccountId} --region us-east-1; then echo "S3 bucket created"; else echo "S3 bucket already exists"; fi

                  # clone custom template repo
                  if [[ -n "${CustomTemplateRepo}" ]]; then
                    # apply git key if entered
                    if [[ -n "${CustomTemplateGitKey}" ]]; then
                      gitkeylocation=/root/.ssh/gitkey
                      gitkey=$(aws --region=${AWS::Region} ssm get-parameters --names "${CustomTemplateGitKey}" --with-decryption | jq --raw-output '.Parameters[0] .Value')
                      echo -e $gitkey > $gitkeylocation
                      chmod 600 $gitkeylocation
                      export GIT_SSH_COMMAND="ssh -o IdentitiesOnly=yes -o StrictHostKeyChecking=no -i $gitkeylocation"
                    fi
                    # clone the repo
                    mkdir /home/forge/custom-templates
                    cd /home/forge/custom-templates
                    git clone "${CustomTemplateRepo}"
                  fi

                  # upload SAML params for later use
                  if [[ -n "${SamlMetadataUrl}" ]]; then
                    aws --region=${region} ssm put-parameter --overwrite --name "atl_forge_secret_key" --type=SecureString --value="${FlaskSecretKey}"
                    aws --region=${region} ssm put-parameter --overwrite --name "atl_forge_saml_metadata_protocol" --type=SecureString --value="${SamlProtocol}"
                    aws --region=${region} ssm put-parameter --overwrite --name "atl_forge_saml_metadata_url" --type=SecureString --value="${SamlUrl}"
                  fi

                  # download config files from S3 if they exist
                  cd /home/forge/atl-cfn-forge
                  aws s3 cp s3://atl-cfn-forge-${AccountId}/config/global-variables.js static/js/
                  aws s3 cp s3://atl-cfn-forge-${AccountId}/config/permissions.json .
                  aws s3 cp --recursive s3://atl-cfn-forge-${AccountId}/config/stacks stacks/
                  aws s3 cp s3://atl-cfn-forge-${AccountId}/config/locking locks/

                  # final chown for updated files
                  chown -R forge:forge /home/forge
                - AccountId: !Ref AWS::AccountId
                  CustomTemplateRepo: !Ref CustomTemplateRepo
                  CustomTemplateGitKey: !Ref CustomTemplateGitKey
                  region: !Ref AWS::Region
                  SamlProtocol: !If [NoSamlMetadata, '', !Select [0, !Split ['://', !Ref SamlMetadataUrl]]]
                  SamlUrl: !If [NoSamlMetadata, '', !Select [1, !Split ['://', !Ref SamlMetadataUrl]]]
              mode: "000700"
              owner: root
              group: root
            /etc/init/forge.conf:
              content: !Sub
                - |
                  #!upstart
                  description "Atlassian Cloudformation Forge"

                  start on started mountall
                  stop on shutdown

                  # Automatically Respawn:
                  respawn
                  respawn limit 3 5

                  script
                    cd /home/forge/atl-cfn-forge
                    exec sudo -u forge /usr/bin/python3 /home/forge/atl-cfn-forge/acforge.py --region=${region} ${noSaml} >> /var/log/forge.log 2>&1
                  end script
                - noSaml: !If [NoSamlMetadata, "--nosaml", '']
                  region: !Ref AWS::Region
              mode: "000600"
              owner: root
              group: root
            /tmp/forge.properties:
              content: !Sub
                - |
                  # Regions in format 'aws_region: region_name'
                  # Names are for reference only, so can be AWS region names or Staging/Production etc
                  # Enter default region first"
                  [regions]
                  ${Regions}

                  [analytics]
                  enabled: ${Analytics}

                  [s3]
                  bucket: atl-cfn-forge-${AWS::AccountId}
                - Analytics: !Ref Analytics
                  Regions: !Join ["\n", !Ref Regions]
              mode: "000750"
              owner: root
              group: root
            /tmp/bootstrap_local_ansible:
              content: !Sub
                - |
                  #!/usr/bin/env bash
                  # bootstrap any local-ansible repo that is passed in on this template

                  localansiblerepopath=/home/localansible
                  gitkeylocation=/root/.ssh/gitkey

                  gitkey=$(aws --region=${AWS::Region} ssm get-parameters --names ${LocalAnsibleGitSshKeyName} --with-decryption 2>/dev/null| jq --raw-output '.Parameters[0] .Value')
                  echo -e $gitkey > $gitkeylocation
                  chmod 600 $gitkeylocation
                  export GIT_SSH_COMMAND="ssh -o IdentitiesOnly=yes -o StrictHostKeyChecking=no -i $gitkeylocation"

                  mkdir -p $localansiblerepopath
                  cd $localansiblerepopath
                  git clone ${LocalAnsibleGitRepo}
                  cp $localansiblerepopath/*/local-ansible-run /usr/local/bin/local-ansible-run
                  chmod 750 /usr/local/bin/local-ansible-run
                - LocalAnsibleGitRepo: !Ref LocalAnsibleGitRepo
                  LocalAnsibleGitSshKeyName: !Ref LocalAnsibleGitSshKeyName
              mode: "000750"
              owner: root
              group: root
            /tmp/update:
              content: !Sub
                - |
                  #!/usr/bin/env bash

                  # update the submodules
                  git pull
                  git submodule update --init --recursive
                  git submodule foreach git checkout master
                  git submodule foreach git pull

                  # update custom template repo
                  if [[ -n "${CustomTemplateRepo}" ]]; then
                    # apply git key if entered
                    if [[ -n "${CustomTemplateGitKey}" ]]; then
                      sshlocation=/home/forge/.ssh
                      gitkeylocation=$sshlocation/gitkey
                      gitkey=$(aws --region=${AWS::Region} ssm get-parameters --names "${CustomTemplateGitKey}" --with-decryption | jq --raw-output '.Parameters[0] .Value')
                      mkdir $sshlocation
                      chmod 770 $sshlocation
                      echo -e $gitkey > $gitkeylocation
                      chmod 700 $sshlocation
                      chmod 600 $gitkeylocation
                      export GIT_SSH_COMMAND="ssh -o IdentitiesOnly=yes -o StrictHostKeyChecking=no -i $gitkeylocation"
                    fi
                    # update the repo
                    cd /home/forge/custom-templates/*/.
                    git checkout master
                    git pull
                  fi
                - CustomTemplateRepo: !Ref CustomTemplateRepo
                  CustomTemplateGitKey: !Ref CustomTemplateGitKey
              mode: "000700"
              owner: root
              group: root
          commands:
            001_enable_epel:
              command: yum-config-manager --enable epel
            002_setup_forge:
              cwd: /root/
              command: ./setup_forge.sh
            003_run_forge:
              command:
                start forge
            090_bootstrap_local_ansible:
              command: /tmp/bootstrap_local_ansible > /var/log/bootstrap_local_ansible.log 2>&1
              ignoreErrors: true
            095_ansible_first_run:
              command: /usr/local/bin/local-ansible-run
              ignoreErrors: true
    Properties:
      AssociatePublicIpAddress: false
      BlockDeviceMappings:
        - DeviceName: /dev/xvda
          Ebs:
            VolumeSize: !Ref NodeVolumeSize
            DeleteOnTermination: true
      IamInstanceProfile: !Ref ForgeNodeInstanceProfile
      ImageId: !FindInMap [RegionAmiMap, !Ref "AWS::Region", "ami"]
      InstanceType: !Ref NodeInstanceType
      KeyName: !Ref KeyName
      SecurityGroups: [!Ref ForgeNodeSecurityGroup]
      UserData:
        Fn::Base64: !Sub |
          #!/bin/bash -xe
          yum update -y aws-cfn-bootstrap
          /opt/aws/bin/cfn-init -v --stack ${AWS::StackName} --resource ForgeNodeLaunchConfig --region ${AWS::Region}
          /opt/aws/bin/cfn-signal -e $? --stack ${AWS::StackName} --resource ForgeNodeGroup --region ${AWS::Region}
  ForgeNodeSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Allow HTTP, SSH and ICMP to Forge node
      VpcId: !Ref VPC
      SecurityGroupIngress:
        - IpProtocol: tcp
          CidrIp: !Ref CidrBlock
          FromPort: 22
          ToPort: 22
        - IpProtocol: tcp
          CidrIp: !Ref CidrBlock
          FromPort: 80
          ToPort: 80
        - IpProtocol: tcp
          FromPort: 8000
          ToPort: 8000
          CidrIp: !Ref CidrBlock
        - IpProtocol: icmp
          FromPort: -1
          ToPort: -1
          CidrIp: !Ref CidrBlock
      Tags:
        - Key: Name
          Value: !Sub ${AWS::StackName}-ForgeNode-SG
  SecurityGroupIngress:
    Type: AWS::EC2::SecurityGroupIngress
    Properties:
      GroupId: !Ref ForgeNodeSecurityGroup
      FromPort: -1
      IpProtocol: -1
      ToPort: -1
      SourceSecurityGroupId: !Ref ForgeNodeSecurityGroup
# Loadbalancer
  LoadBalancer:
    Type: AWS::ElasticLoadBalancing::LoadBalancer
    Properties:
      CrossZone: true
      Listeners:
        - LoadBalancerPort: 80
          Protocol: HTTP
          InstancePort: 8000
          InstanceProtocol: HTTP
      HealthCheck:
        Target: 'HTTP:8000/status'
        Timeout: 29
        Interval: 30
        UnhealthyThreshold: 2
        HealthyThreshold: 2
      Scheme: !If [InternetAccessible, 'internet-facing', 'internal']
      SecurityGroups: [!Ref ForgeNodeSecurityGroup]
      Subnets: !Ref ExternalSubnet
      Tags:
        - Key: Name
          Value: !Sub ["${StackName}-LoadBalancer", StackName: !Ref 'AWS::StackName']
  LoadBalancerCname:
    Condition: UseHostedZone
    Type: AWS::Route53::RecordSet
    Properties:
      HostedZoneName: !Ref HostedZone
      Comment: Route53 cname for the ELB
      Name: !Join ['.', [!Ref "AWS::StackName", !Ref 'HostedZone']]
      Type: CNAME
      TTL: 900
      ResourceRecords:
        - !GetAtt LoadBalancer.DNSName
Outputs:
  ServiceURL:
    Description: The URL to access this Atlassian service
    Value: !If
      - UseHostedZone
      - !Sub
        - "http://${LBCName}"
        - LBCName: !Ref LoadBalancerCname
      - !Sub
        - "http://${LoadBalancerDNSName}"
        - LoadBalancerDNSName: !GetAtt LoadBalancer.DNSName
