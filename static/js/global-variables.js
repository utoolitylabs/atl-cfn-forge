// These values can be used to automatically populate template parameters
// This means you don't have to always copy/paste VPCs, subnets, hostedzones or ssh keys into the fields

// VPCs
// format vpc-56abc789
const us_east_1_default_vpc = "";
const us_west_2_default_vpc = "";

// Subnets
// format 'subnet-12abc345,subnet-12abc346'
const us_east_1_default_subnets = "";
const us_west_2_default_subnets = "";

// Hosted Zone
// format 'myteam.example.com.'
const hosted_zone = "";

// SSH Key
const ssh_key_name = "";